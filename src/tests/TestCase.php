<?php

namespace Tests;

require_once __DIR__ . "/../../vendor/autoload.php";

use App\Instagram\InstagramAppApi;
use Faker\Provider\Base;
use Faker\Provider\Image;
use Faker\Provider\Internet;
use Faker\Provider\Lorem;
use Faker\Provider\Miscellaneous;
use Faker\Provider\Person;
use Faker\Provider\PhoneNumber;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Faker\Generator;

abstract class TestCase extends BaseTestCase
{
    /** @var Generator */
    protected $faker;

    protected function setUp()
    {
        parent::setUp();
        $this->setUpFaker();
    }

    protected function setUpFaker(){
        $this->faker = new Generator();
        $this->faker->addProvider(new Base($this->faker));
        $this->faker->addProvider(new Person($this->faker));
        $this->faker->addProvider(new PhoneNumber($this->faker));
        $this->faker->addProvider(new Internet($this->faker));
        $this->faker->addProvider(new Image($this->faker));
        $this->faker->addProvider(new Miscellaneous($this->faker));
        $this->faker->addProvider(new Lorem($this->faker));
    }

    public function assertJsonStructure($structure, $data)
    {
        foreach ($structure as $key => $value){
            if(is_array($value)){
                $this->assertArrayHasKey($key, $data);
                $this->assertJsonStructure($structure[$key], $data[$key]);
            }
            else{
                $this->assertArrayHasKey($value, $data);
            }
        }
    }
}