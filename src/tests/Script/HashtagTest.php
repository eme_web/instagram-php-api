<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/1/19
 * Time: 4:08 PM
 */

namespace Tests\Script;

use App\Instagram\Requests\Hashtag;
use Mockery;


class HashtagTest extends ScriptTest
{
    /** @var \Mockery\Mock */
    protected $mockHashtag;

    protected $tag;

    protected $hashtagFeed;

    protected function setUp()
    {
        parent::setUp();

        $this->mockHashtag = Mockery::mock(Hashtag::class);

        $this->hashtag = $this->faker->name;
        $items = [
            "id" => $this->faker->numberBetween(0, 100)
        ];
        $this->hashtagFeed = [
            'ranked_items' => $items,
            'items' => $items
        ];
    }

    public function testGetsTagFeed()
    {
        $hashtag = "tag";

        $mockResponse = $this->_getMockResponse(200, $this->hashtagFeed);
        $this->mockHashtag->shouldReceive('getTagFeed')->with($hashtag)->andReturn($mockResponse);
        $this->api->hashtag = $this->mockHashtag;

        $this->_executeCommandAndAssert("getTagFeed -u username -p password --hashtag $hashtag", $this->hashtagFeed);
        $this->api->hashtag->shouldHaveReceived('getTagFeed', [$hashtag]);
    }

}