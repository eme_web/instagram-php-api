<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/5/19
 * Time: 5:14 PM
 */

namespace Tests\Script;


use App\Instagram\InstagramAppApi;
use App\Instagram\Requests\Account;
use Mockery;

class ProxyTest extends ScriptTest
{
    /** @var Mockery\Mock */
    protected $mockApi;

    protected $proxyElements;
    protected $proxy;
    protected $mockUserData;

    protected function setUp()
    {
        parent::setUp();
        $this->api = Mockery::mock(InstagramAppApi::class)->makePartial();
        $this->api->account = Mockery::mock(Account::class);

        $this->proxyElements = ["http://", $this->faker->ipv4, $this->faker->numberBetween(80, 1000)];

        $this->proxy = $this->proxyElements[0].$this->proxyElements[1].":".$this->proxyElements[2];
        $this->mockUserData = [ 'user' =>[
            'pk' => strval($this->faker->numberBetween(1, 1000)),
            'username' => $this->faker->userName,
            'email' => $this->faker->safeEmail,
            'profile_pic_url' => $this->faker->imageUrl(),
            'country_code' => $this->faker->numberBetween(1, 100),
            'national_number' => $this->faker->phoneNumber,
            'phone_number' => $this->faker->phoneNumber,
            'full_name' => $this->faker->name
        ]];
        $this->api->shouldReceive("setProxy");
        $mockResponse = $this->_getMockResponse(200, $this->mockUserData);

        $this->api->account->shouldReceive("login")->andReturn(null);
        $this->api->account->shouldReceive("getSelfUsernameInfo")->andReturn($mockResponse);
    }

    public function testSetsProxy(){
        $command = "getSelfUsernameInfo -u username -p password --proxy $this->proxy";
        $this->_executeCommandAndAssert($command, $this->mockUserData);
        $this->api->shouldHaveReceived("setProxy", [$this->proxyElements[1], $this->proxyElements[2], false]);
    }





}