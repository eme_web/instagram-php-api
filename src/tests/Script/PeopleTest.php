<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/4/19
 * Time: 4:12 PM
 */

namespace Tests\Script;

use App\Instagram\Requests\People;
use Mockery;

class PeopleTest extends ScriptTest
{
    /** @var Mockery\Mock */
    protected $peopleMock;

    protected $username;
    protected $password;

    protected $userId;
    protected $accountName;
    protected $maxId;

    protected $mockUserData;
    protected $followAndUnfollowResponse;
    protected $followsAndFollowings;
    protected $friendshipStatus;
    protected $mediaLikers;

    protected function setUp()
    {
        parent::setUp();

        $this->peopleMock = Mockery::mock(People::class);

        $this->username = $this->faker->userName;
        $this->password = $this->faker->password;

        $this->userId = strval($this->faker->numberBetween(1, 1000));
        $this->accountName = $this->faker->userName;
        $this->maxId = $this->faker->numberBetween(0, 100);

        $this->mockUserData = [ 'user' =>[
            'pk' => strval($this->faker->numberBetween(1, 1000)),
            'username' => $this->faker->userName,
            'email' => $this->faker->safeEmail,
            'profile_pic_url' => $this->faker->imageUrl(),
            'country_code' => $this->faker->numberBetween(1, 100),
            'national_number' => $this->faker->phoneNumber,
            'phone_number' => $this->faker->phoneNumber,
            'full_name' => $this->faker->name
        ]];

        $friendshipStatus = [
            'following' => true,
            'followed_by' => true,
            'incoming_request' => false,
            'outgoing_request' => false,
            'is_private' => false,
            'blocking' => false
        ];

        $this->followAndUnfollowResponse = [
            "friendship_status" => $friendshipStatus
        ];

        $this->friendshipStatus = $friendshipStatus;


        $this->followsAndFollowings = [
            "users" => [
                $this->mockUserData
            ],
            "next_max_id" => $this->faker->text(20),
            'page_size' => $this->faker->numberBetween(1, 200),
            'big_list' => true
        ];

    }

    public function testGetsUsernameInfo()
    {
        $mockResponse = $this->_getMockResponse(200, $this->mockUserData);
        $this->peopleMock->shouldReceive('getUsernameInfo')->withArgs([$this->accountName])->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getUsernameInfo -u username -p password --accountname $this->accountName", $this->mockUserData);
        $this->peopleMock->shouldHaveReceived('getUsernameInfo', [$this->accountName]);
    }

    public function testGetUserInfoById(){
        $mockResponse = $this->_getMockResponse(200, $this->mockUserData);
        $this->peopleMock->shouldReceive('getUserInfoById')->withArgs([$this->userId])->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getUserInfoById -u username -p password --userid $this->userId", $this->mockUserData);
        $this->peopleMock->shouldHaveReceived('getUserInfoById', [$this->userId]);
    }

    public function testFollow(){
        $mockResponse = $this->_getMockResponse(200, $this->followAndUnfollowResponse);
        $this->peopleMock->shouldReceive('follow')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("follow -u username -p password --userid $this->userId", $this->followAndUnfollowResponse);
        $this->peopleMock->shouldHaveReceived('follow', [$this->userId]);
    }

    public function testUnfollow(){
        $mockResponse = $this->_getMockResponse(200, $this->followAndUnfollowResponse);
        $this->peopleMock->shouldReceive('unfollow')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("unfollow -u username -p password --userid $this->userId", $this->followAndUnfollowResponse);
        $this->peopleMock->shouldHaveReceived('unfollow', [$this->userId]);
    }

    public function testGetsUserFollowers(){
        $mockResponse = $this->_getMockResponse(200, $this->followsAndFollowings);
        $this->peopleMock->shouldReceive('getFollowers')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getFollowers -u username -p password --userid $this->userId", $this->followsAndFollowings);
        $this->peopleMock->shouldHaveReceived('getFollowers', [$this->userId, null]);
    }

    public function testGetsUserFollowings(){
        $mockResponse = $this->_getMockResponse(200, $this->followsAndFollowings);
        $this->peopleMock->shouldReceive('getFollowing')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getFollowing -u username -p password --userid $this->userId", $this->followsAndFollowings);
        $this->peopleMock->shouldHaveReceived('getFollowing', [$this->userId, null]);
    }

    public function testGetsSelfUserFollowings()
    {
        $mockResponse = $this->_getMockResponse(200, $this->followsAndFollowings);
        $this->peopleMock->shouldReceive('getSelfFollowing')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getSelfFollowing -u username -p password", $this->followsAndFollowings);
        $this->peopleMock->shouldHaveReceived('getSelfFollowing', [null]);
    }

    public function testGetsSelfUserFollowers()
    {
        $mockResponse = $this->_getMockResponse(200, $this->followsAndFollowings);
        $this->peopleMock->shouldReceive('getSelfFollowers')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getSelfFollowers -u username -p password", $this->followsAndFollowings);
        $this->peopleMock->shouldHaveReceived('getSelfFollowers', [null]);
    }

    public function testGetsSelfFollowersOrFollowingsWithMaxId()
    {
        $mockResponse = $this->_getMockResponse(200, $this->followsAndFollowings);
        $this->peopleMock->shouldReceive('getSelfFollowers')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getSelfFollowers -u username -p password --maxid $this->maxId", $this->followsAndFollowings);
        $this->peopleMock->shouldHaveReceived('getSelfFollowers', [$this->maxId]);
    }

    public function testGetsFollowersOrFollowingsWithMaxId()
    {
        $mockResponse = $this->_getMockResponse(200, $this->followsAndFollowings);
        $this->peopleMock->shouldReceive('getFollowers')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getFollowers -u username -p password --userid $this->userId --maxid $this->maxId", $this->followsAndFollowings);
        $this->peopleMock->shouldHaveReceived('getFollowers', [$this->userId, $this->maxId]);
    }

    public function testGetsFriendshipStatus()
    {
        $mockResponse = $this->_getMockResponse(200, $this->friendshipStatus);
        $this->peopleMock->shouldReceive('getFriendshipStatus')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getFriendshipStatus -u username -p password --userid $this->userId", $this->friendshipStatus);
        $this->peopleMock->shouldHaveReceived('getFriendshipStatus', [$this->userId]);
    }

    public function testIgnoresNonNeededParameters()
    {
        $mockResponse = $this->_getMockResponse(200, $this->friendshipStatus);
        $this->peopleMock->shouldReceive('getFriendshipStatus')->andReturn($mockResponse);
        $this->api->people = $this->peopleMock;

        $this->_executeCommandAndAssert("getFriendshipStatus -u username -p password --userid $this->userId --maxid $this->maxId", $this->friendshipStatus);
        $this->peopleMock->shouldHaveReceived('getFriendshipStatus', [$this->userId]);
    }

}