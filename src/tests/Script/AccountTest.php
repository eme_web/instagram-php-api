<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/8/19
 * Time: 10:07 AM
 */

namespace Tests\Script;

use App\Instagram\Requests\Account;
use Mockery;

class AccountTest extends ScriptTest
{
    /** @var Mockery\Mock */
    protected $accountMock;

    protected $mockUserData;
    protected $mockUserInfo;

    protected function setUp()
    {
        parent::setUp();
        $this->mockUserData = [ 'user' =>[
            'pk' => strval($this->faker->numberBetween(1, 1000)),
            'username' => $this->faker->userName,
            'email' => $this->faker->safeEmail,
            'profile_pic_url' => $this->faker->imageUrl(),
            'country_code' => $this->faker->numberBetween(1, 100),
            'national_number' => $this->faker->phoneNumber,
            'phone_number' => $this->faker->phoneNumber,
            'full_name' => $this->faker->name,
        ]];

        $this->mockUserInfo = [ 'user' => [
            'following_count' => $this->faker->numberBetween(0, 100),
            'follower_count' => $this->faker->numberBetween(0, 100),
        ]];
    }

    public function testLogin(){
        $mockLoginResponse = ["status" => "ok", "full_login" => true];
        $mockResponse = $this->_getMockResponse(200, $mockLoginResponse);

        $this->accountMock = Mockery::mock(Account::class);
        $this->accountMock->shouldReceive('login')->andReturn($mockResponse);
        $this->api->account = $this->accountMock;

        $this->_executeCommandAndAssert('login -u username -p password', $mockLoginResponse);
        $this->accountMock->shouldHaveReceived('login', ["username", "password"]);
    }

    public function testGetsCurrentUser()
    {
        $mockResponse = $this->_getMockResponse(200, $this->mockUserData);
        $this->mockAccount->shouldReceive('getCurrentUser')->andReturn($mockResponse);
        $this->api->account = $this->mockAccount;

        $this->_executeCommandAndAssert('getCurrentUser -u username -p password', $this->mockUserData);
        $this->mockAccount->shouldHaveReceived('getCurrentUser', []);
    }

    public function testGetsSelfUsernameInfo()
    {
        $mockResponse = $this->_getMockResponse(200, $this->mockUserInfo);
        $this->mockAccount->shouldReceive('getSelfUsernameInfo')->andReturn($mockResponse);
        $this->api->account = $this->mockAccount;

        $this->_executeCommandAndAssert('getSelfUsernameInfo -u username -p password', $this->mockUserInfo);
        $this->mockAccount->shouldHaveReceived('getSelfUsernameInfo', []);
    }

}