<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/13/19
 * Time: 9:47 AM
 */

namespace Tests\Script;

use App\Instagram\Requests\Timeline;
use Mockery;


class TimelineTest extends ScriptTest
{
    /** @var Mockery\Mock */
    protected $timelineMock;

    protected $mockUserFeed;


    protected function setUp()
    {
        parent::setUp();
        $items = [
            "id" => $this->faker->numberBetween(0, 100)
        ];

        $this->mockUserFeed = [
            "items" => $items
        ];

        $this->timelineMock = Mockery::mock(Timeline::class);

    }

    public function testGetsSelfUserFeed(){

        $mockResponse = $this->_getMockResponse(200, $this->mockUserFeed);
        $this->timelineMock->shouldReceive("getSelfUserFeed")->andReturn($mockResponse);
        $this->api->timeline = $this->timelineMock;

        $this->_executeCommandAndAssert("getSelfUserFeed -u username -p password", $mockResponse);
        $this->timelineMock->shouldHaveReceived('getSelfUserFeed', [null]);

    }

}