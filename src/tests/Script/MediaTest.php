<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/1/19
 * Time: 4:08 PM
 */

namespace Tests\Script;

use App\Instagram\Requests\Media;
use Mockery;

class MediaTest extends ScriptTest
{
    /** @var Mockery\Mock */
    protected $mockMedia;

    protected $mediaId;
    protected $mediaInfo;
    protected $mediaLikers;

    protected function setUp()
    {
        parent::setUp();

        $this->mockMedia = Mockery::mock(Media::class);

        $this->mediaId = $this->faker->numberBetween(1,10);
        $this->mediaInfo = ["items" => [
            ["has_liked" => false]
        ]];
        $this->mediaLikers = [
            "user_count" => $this->faker->numberBetween(0,100),
            "users" => []
        ];
    }

    public function testGetsMediaInfo()
    {

        $mockResponse = $this->_getMockResponse(200, $this->mediaInfo);
        $this->mockMedia->shouldReceive('getMediaInfo')->with($this->mediaId)->andReturn($mockResponse);
        $this->api->media = $this->mockMedia;

        $this->_executeCommandAndAssert("getMediaInfo -u username -p password --mediaid $this->mediaId", $this->mediaInfo);
        $this->mockMedia->shouldHaveReceived('getMediaInfo', [$this->mediaId]);
    }

    public function testLikesMedia(){
        $mockData = [];
        $mockResponse = $this->_getMockResponse(200, $mockData);
        $this->mockMedia->shouldReceive('like')->with($this->mediaId)->andReturn($mockResponse);
        $this->api->media = $this->mockMedia;

        $this->_executeCommandAndAssert("like -u username -p password --mediaid $this->mediaId", $mockData);
        $this->mockMedia->shouldHaveReceived('like', [$this->mediaId]);
    }
    public function testGetsMediaLikers()
    {
        $mockResponse = $this->_getMockResponse(200, $this->mediaLikers);
        $this->mockMedia->shouldReceive('getMediaLikers')->andReturn($mockResponse);
        $this->api->media = $this->mockMedia;

        $this->_executeCommandAndAssert("getMediaLikers -u username -p password --mediaid $this->mediaId", $this->mediaLikers);
        $this->mockMedia->shouldHaveReceived('getMediaLikers', [$this->mediaId]);
    }

}