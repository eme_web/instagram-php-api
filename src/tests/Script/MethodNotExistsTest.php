<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/4/19
 * Time: 4:14 PM
 */

namespace Tests\Script;


use App\Console\Exceptions\UnknownCommandException;

class MethodNotExistsTest extends ScriptTest
{
    public function testCallNotExistingMethod()
    {
        $this->expectException(UnknownCommandException::class);
        $console = new \App\Console\ConsoleBootstrap($this->api);
        $console->process('NotExistingMethod -u username -p password');
    }

}