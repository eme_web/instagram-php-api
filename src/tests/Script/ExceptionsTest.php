<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/8/19
 * Time: 11:34 AM
 */

namespace Tests\Script;

use App\Instagram\Exceptions\AccountDeletionRequestException;
use App\Instagram\Exceptions\ActionNotAllowedException;
use App\Instagram\Exceptions\BlockException;
use App\Instagram\Exceptions\DisabledAccountException;
use App\Instagram\Exceptions\FeatureNotAvailableException;
use App\Instagram\Exceptions\InvalidRankTokenException;
use App\Instagram\Exceptions\LinkNotAllowedException;
use App\Instagram\Exceptions\MaxAccountsFollowedException;
use App\Instagram\Exceptions\MediaNotFoundException;
use App\Instagram\Exceptions\ProxyException;
use App\Instagram\Exceptions\RestrictedProfileException;
use App\Instagram\Exceptions\TwoFactorRequiredException;
use App\Instagram\Exceptions\UserRestrictedException;
use App\Instagram\Requests\Account;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use InstagramAPI\Exception\AccountDisabledException;
use InstagramAPI\Exception\ChallengeRequiredException;
use InstagramAPI\Exception\CheckpointRequiredException;
use InstagramAPI\Exception\EmptyResponseException;
use InstagramAPI\Exception\ForcedPasswordResetException;
use InstagramAPI\Exception\IncorrectPasswordException;
use InstagramAPI\Exception\InvalidUserException;
use InstagramAPI\Exception\LoginRequiredException;
use InstagramAPI\Exception\NetworkException;
use InstagramAPI\Exception\NotFoundException;
use InstagramAPI\Exception\SentryBlockException;
use InstagramAPI\Exception\ThrottledException;
use InstagramAPI\Response\LoginResponse;
use Mockery;

class ExceptionsTest extends ScriptTest
{

    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * @param string|\Exception $exceptionClass
     */
    protected function _setMockException($exceptionClass){
        $mock = Mockery::mock(Account::class);
        $mock->expects("login")->andThrow($exceptionClass);
        $this->api->account = $mock;
    }

    protected function _assertExceptionResponse($response, $status, $code)
    {
        $response_array = json_decode($response, true);
        $this->assertArraySubset(["status" => $status, "code" => $code], $response_array);
    }

    /**
     * @param string|\Exception $exceptionClass
     * @param $expectedStatus
     * @param $expectedCode
     */
    protected function _testExceptionOutput($exceptionClass, $expectedStatus, $expectedCode)
    {
        $this->_setMockException($exceptionClass);

        $response = $this->_executeCommand('getSelfUsernameInfo -u username -p password');
        $this->_assertExceptionResponse($response, $expectedStatus, $expectedCode);
    }

    public function testInvalidUserException()
    {
        $this->_testExceptionOutput(InvalidUserException::class, 404, "invalid_user");
    }

    public function testLoginRequiredException()
    {
        $this->_testExceptionOutput(LoginRequiredException::class, 400, "login_required");
    }

    public function testIncorrectPasswordException()
    {
        $this->_testExceptionOutput(IncorrectPasswordException::class, 400, "incorrect_password");
    }

    public function testResetPasswordException()
    {
        $this->_testExceptionOutput(ForcedPasswordResetException::class, 400, "reset_password");
    }

    public function testCheckpointRequiredException()
    {
        $this->_testExceptionOutput(CheckpointRequiredException::class, 400, "checkpoint_required");
    }

    public function testChallengeRequiredException()
    {
        $this->_testExceptionOutput(ChallengeRequiredException::class, 400, "challenge_required");
    }

    public function testNetworkException()
    {
        $request = new Request("", "");
        $clientException = new ClientException("", $request);
        $networkException = new NetworkException($clientException);
        $this->_testExceptionOutput($networkException, 400, "network_exception");
    }

    public function testEmptyResponseException()
    {
        $this->_testExceptionOutput(EmptyResponseException::class, 400, "empty_response");
    }

    public function testBlockException()
    {
        $this->_testExceptionOutput(BlockException::class, 400, "blocked");
    }

    public function testThrottledException()
    {
        $this->_testExceptionOutput(ThrottledException::class, 429, "throttled");
    }

    public function testMaxAccountsFollowedException()
    {
        $this->_testExceptionOutput(MaxAccountsFollowedException::class, 400, "max_accounts_followed");
    }

    public function testActionNotAllowedException(){
        $this->_testExceptionOutput(ActionNotAllowedException::class, 400, "action_not_allowed");
    }

    public function testFeatureNotAvailableException(){
        $this->_testExceptionOutput(FeatureNotAvailableException::class, 400, "feature_not_available");
    }

    public function testMediaNotFoundException(){
        $this->_testExceptionOutput(MediaNotFoundException::class, 404, "media_not_found");
    }

    public function testSentryBlockException(){
        $this->_testExceptionOutput(SentryBlockException::class, 400, "sentry_block");
    }

    public function testProxyException(){
        $this->_testExceptionOutput(ProxyException::class, 400, "proxy_error");
    }

    public function testTwoFactorRequiredException(){
        $loginResponse = new LoginResponse();
        $this->_testExceptionOutput(new TwoFactorRequiredException($loginResponse), 400, "two_factor_required");
    }

    public function testNotFoundException(){
        $this->_testExceptionOutput(NotFoundException::class, 400, "not_found_exception");
    }

    public function testInvalidRankToken(){
        $this->_testExceptionOutput(InvalidRankTokenException::class, 400, "invalid_rank_token");
    }

    public function testUserRestricted(){
        $this->_testExceptionOutput(UserRestrictedException::class, 400, "user_restricted");
    }

    public function testAccountDeletionRequested(){
        $this->_testExceptionOutput(AccountDeletionRequestException::class, 400, "account_deletion_requested");
    }

    public function testDisabledAccount(){
        $this->_testExceptionOutput(AccountDisabledException::class, 400, "disabled_account");
    }

    public function testLinkNotAllowed(){
        $this->_testExceptionOutput(LinkNotAllowedException::class, 400, "link_not_allowed");
    }

    public function testRestrictedProfile(){
        $this->_testExceptionOutput(RestrictedProfileException::class, 400, "restricted_profile");
    }

}