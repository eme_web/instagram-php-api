<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/13/19
 * Time: 9:45 AM
 */

namespace Tests\InstagramApi;


class TimelineTest extends InstagramTest
{
    protected $userFeedStructure;

    protected function setUp()
    {
        parent::setUp();
        $this->api->account->login($this->username, $this->password);
        $this->userFeedStructure = [
            'items' => [
                ['pk', 'id', 'media_type', 'has_liked']
            ],
            'num_results',
            'more_available',
            'next_max_id',
            'auto_load_more_enabled',
        ];
    }

    public function testGetSelfUserFeed(){
        $response = $this->api->timeline->getSelfUserFeed();
        $this->assertJsonStructure($this->userFeedStructure, $response->asArray());
    }

}