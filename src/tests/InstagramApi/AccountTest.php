<?php
/**
 * Created by PhpStorm.
 * UserClass: eme
 * Date: 1/24/19
 * Time: 3:31 PM
 */

namespace Tests\InstagramApi;


class AccountTest extends InstagramTest
{

    protected function setUp()
    {
        parent::setUp();

    }

    public function testLogin(){
        $response = $this->api->account->login($this->username, $this->password);
        $expectedResponse = ["status" => "ok"];
        $this->assertArraySubset($expectedResponse, $response->asArray());
    }

    public function testGetCurrentUser(){
        $this->api->account->login($this->username, $this->password);
        $response = $this->api->account->getCurrentUser();
        $structure = [
            'user' => [
                'pk',
                'username',
                'email',
                'profile_pic_url',
                'country_code',
                'national_number',
                'phone_number',
                'full_name',
            ]
        ];
        $this->assertJsonStructure($structure, $response->asArray());
    }

    public function testGetSelfInfo(){

        $this->api->account->login($this->username, $this->password);
        $response = $this->api->account->getSelfUsernameInfo();
        $structure = [
            'user' => [
                'pk',
                'username',
                'profile_pic_url',
                'following_count',
                'follower_count',
                'is_private'
            ]
        ];
        $this->assertJsonStructure($structure, $response->asArray());
    }

}