<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/4/19
 * Time: 4:47 PM
 */

namespace Tests\InstagramApi;


class PeopleTest extends InstagramTest
{
    protected $targetAccount;
    protected $targetUsername;
    protected $friendshipStructure;
    protected $unwrappedFriendshipStructure;
    protected $followersAndFollowingsStructure;
    protected $userInfoStructure;
    protected $searchResponseStructure;

    protected function setUp()
    {
        parent::setUp();
        $this->targetAccount = 389937097;
        $this->targetUsername = "davidperezcol";

        $this->api->account->login($this->username, $this->password);
        $friendshipStructure = [
            'following',
            'followed_by',
            'incoming_request',
            'outgoing_request',
            'is_private',
            'blocking',
        ];
        $this->friendshipStructure = [
            'friendship_status' => $friendshipStructure
        ];

        $this->unwrappedFriendshipStructure = $friendshipStructure;

        $this->followersAndFollowingsStructure = [
            'users',
            'next_max_id',
            'page_size',
            'big_list',
        ];

        $this->userInfoStructure = [
            "user" => [
                "pk",
                "username",
                "profile_pic_url",
                "following_count",
                "follower_count",
                "is_private"
            ]
        ];

        $this->searchResponseStructure = [
            "users" => [
                ["pk",
                "username",
                "is_private",
                "profile_pic_url",
                "follower_count"]
            ]
        ];


    }

    public function testFollow()
    {
        $response = $this->api->people->follow($this->targetAccount);
        $this->assertJsonStructure($this->friendshipStructure, $response->asArray());
    }

    public function testUnfollow()
    {
        $response = $this->api->people->unfollow($this->targetAccount);
        $this->assertJsonStructure($this->friendshipStructure, $response->asArray());
    }

    public function testGetUsernameInfo(){
        $response = $this->api->people->getUsernameInfo($this->targetUsername);
        $this->assertJsonStructure($this->userInfoStructure, $response->asArray());
    }

    public function testGetUserInfoById(){
        $response = $this->api->people->getUserInfoById($this->targetAccount);
        $this->assertJsonStructure($this->userInfoStructure, $response->asArray());
    }

    public function testGetUserFollowers()
    {
        $response = $this->api->people->getFollowers($this->targetAccount);
        $this->assertJsonStructure($this->followersAndFollowingsStructure, $response->asArray());
    }

    public function testGetUserFollowings()
    {
        $response = $this->api->people->getFollowing($this->targetAccount);
        $this->assertJsonStructure($this->followersAndFollowingsStructure, $response->asArray());
    }

    public function testGetSelfUserFollowers()
    {
        $response = $this->api->people->getSelfFollowers();
        $this->assertJsonStructure($this->followersAndFollowingsStructure, $response->asArray());
    }

    public function testGetSelfUserFollowings()
    {
        $response = $this->api->people->getSelfFollowing();
        $this->assertJsonStructure($this->followersAndFollowingsStructure, $response->asArray());
    }

    public function testGetFollowersOrFollowingsWithFetchPage()
    {
        $first_fetch = $this->api->people->getFollowers($this->targetAccount);
        $first_users = $first_fetch->getUsers();
        $next_max_id = $first_fetch->getNextMaxId();

        sleep(10);
        $second_fetch = $this->api->people->getFollowers($this->targetAccount, $next_max_id);
        $second_users = $second_fetch->getUsers();

        $this->assertNotEquals($first_users, $second_users);
    }

    public function testGetFriendshipStatus()
    {
        $response = $this->api->people->getFriendshipStatus($this->targetAccount);
        $this->assertJsonStructure($this->unwrappedFriendshipStructure, $response->asArray());
    }

    public function testSearchUser(){
        $response = $this->api->people->searchUser($this->username);
        $this->assertJsonStructure($this->searchResponseStructure, $response->asArray());
    }

}