<?php
/**
 * Created by PhpStorm.
 * UserClass: eme
 * Date: 1/24/19
 * Time: 3:34 PM
 */

namespace Tests\InstagramApi;


use App\Instagram\InstagramAppApi;
use Tests\TestCase;
use Dotenv\Dotenv;

abstract class InstagramTest extends TestCase
{
    protected $username = "pruebacrono";
    protected $password = "Jupiter01";

    /** @var InstagramAppApi */
    protected $api;

    protected function setUp()
    {
        parent::setUp();

        $dotenv = new Dotenv(__DIR__."/../../");
        $dotenv->load();

        $storageConfig = [
            "storage" => "mysql",
            "dbusername" => getenv("DB_USERNAME"),
            "dbpassword" => getenv("DB_PASSWORD"),
            "dbhost" => getenv("DB_HOST"),
            "dbname" => getenv("DB_NAME"),
            "dbtablename" => "instagram_sessions_storage"
        ];

        $this->api = new InstagramAppApi(false, $storageConfig);
    }

    protected function tearDown()
    {
        sleep(10);
        parent::tearDown();
    }

}