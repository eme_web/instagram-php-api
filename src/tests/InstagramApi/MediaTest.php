<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 1/31/19
 * Time: 5:44 PM
 */

namespace Tests\InstagramApi;


class MediaTest extends InstagramTest
{

    protected $mediaId;

    protected function setUp()
    {
        parent::setUp();
        $this->mediaId = "1956991169615719750_1297406006";
        $this->api->account->login($this->username, $this->password);
    }

    public function testGetsMediaInfo(){
        $response = $this->api->media->getMediaInfo($this->mediaId);
        $structure = [
            'items' => [
                ["has_liked"]
            ]
        ];
        $this->assertJsonStructure($structure, $response->asArray());
    }

    public function testGetsMediaLikers(){
        $response = $this->api->media->getMediaLikers($this->mediaId);
        $structure = [
            'user_count',
            'users'
        ];
        $this->assertJsonStructure($structure, $response->asArray());
    }

    public function testLike(){
        $response = $this->api->media->like($this->mediaId);
        $structure = [
            'status'
        ];
        $this->assertJsonStructure($structure, $response->asArray());
    }

    public function testUnlike(){
        $response = $this->api->media->unlike($this->mediaId);
        $structure = [
            'status'
        ];
        $this->assertJsonStructure($structure, $response->asArray());
    }

}