<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/8/19
 * Time: 10:47 AM
 */

namespace Tests\InstagramApi;


class HashtagTest extends InstagramTest
{
    protected $hashtag;

    protected function setUp()
    {
        parent::setUp();
        $this->hashtag = "dogs";
        $this->api->account->login($this->username, $this->password);
    }

    public function testGetsHashtagFeed(){
        $response = $this->api->hashtag->getTagFeed($this->hashtag);
        $structure = [
            'ranked_items' => [
                ['pk', 'id', 'media_type', 'has_liked', 'user']
            ],
            'items' => [
                ['pk', 'id', 'media_type', 'has_liked', 'user']
            ],
            'next_max_id'
        ];
        $this->assertJsonStructure($structure, $response->asArray());
    }

    public function testSearchHashtag(){
        $structure = [
            "results" => [
                ["id", "name", "media_count"]
            ]
        ];
        $response = $this->api->hashtag->searchHashtag($this->hashtag);
        $this->assertJsonStructure($structure, $response->asArray());
    }

}