<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/21/19
 * Time: 9:55 AM
 */

namespace Tests\InstagramApi;


use App\Instagram\Exceptions\DisabledAccountException;
use App\Instagram\Exceptions\InvalidRankTokenException;
use App\Instagram\Exceptions\ProxyException;
use App\Instagram\Exceptions\RestrictedProfileException;
use InstagramAPI\Exception\InvalidUserException;
use InstagramAPI\Exception\NotFoundException;

class ExceptionTest extends InstagramTest
{
    private $damagedProxy;
    private $invalidUser;
    private $nonExistentUserId;
    protected $targetAccount;
    protected $restrictedUserId;

    protected function setUp()
    {
        parent::setUp();
        $this->damagedProxy = ["ip" => "62.210.106.211", "port" => "10123"];
        $this->invalidUser = ["username" => "alksjfhdkjsdhkkjkkjkjquiquuiquqiuq", "password" => "123"];
        $this->nonExistentUserId = "1833960321";
        $this->restrictedUserId = "4561193519";
        $this->targetAccount = 389937097;
    }

    public function testProxyError(){
        $this->expectException(ProxyException::class);
        $this->api->account->login($this->username, $this->password);
        $this->api->setProxy($this->damagedProxy["ip"], $this->damagedProxy["port"]);
        $this->api->people->getUserInfoById($this->targetAccount);
    }

    public function testInvalidUser(){
        $this->expectException(InvalidUserException::class);
        $this->api->account->login($this->invalidUser["username"], $this->invalidUser["password"]);
    }

    public function testActionOnNonExistentUser(){
        $this->expectException(NotFoundException::class);
        $this->api->account->login($this->username, $this->password);
        $this->api->people->getUserInfoById($this->nonExistentUserId);
    }

    public function testUserFriendshipOnRestrictedUser(){
        $this->expectException(RestrictedProfileException::class);
        $this->api->account->login($this->username, $this->password);
        $this->api->people->getFriendshipStatus($this->restrictedUserId);
    }

}