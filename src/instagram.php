<?php

require_once __DIR__ . "/../vendor/autoload.php";

use App\Instagram\InstagramAppApi;
use InstagramAPI\Exception\SettingsException;
use App\Console\ApiException;
use Dotenv\Dotenv;

$dotenv = new Dotenv(__DIR__);
$dotenv->load();

try{
    $storageConfig = getDatabaseParameters();
    $console = new \App\Console\ConsoleBootstrap(new InstagramAppApi(false, $storageConfig));
    $response = $console->process()->toString();
}
catch (SettingsException $settingsException){
    $exceptionHandler = new ApiException($settingsException);
    $response = $exceptionHandler->toString();
}
catch (\App\Console\MethodNotFoundException $e){
    echo $e->getMessage();
}

echo $response;


function getDatabaseParameters(){
    return [
        "storage" => "mysql",
        "dbusername" => getenv("DB_USERNAME"),
        "dbpassword" => getenv("DB_PASSWORD"),
        "dbhost" => getenv("DB_HOST"),
        "dbname" => getenv("DB_NAME"),
        "dbtablename" => "instagram_sessions_storage"
    ];
}