<?php

namespace App\Instagram;

use App\Instagram\Requests\Account;
use App\Instagram\Requests\Hashtag;
use App\Instagram\Requests\Media;
use App\Instagram\Requests\People;
use App\Instagram\Requests\Timeline;
use Dotenv\Dotenv;
use InstagramAPI\Instagram;

class InstagramAppApi
{
    /** @var Instagram */
    private $api;

    /** @var Account */
    public $account;

    /** @var Hashtag */
    public $hashtag;

    /** @var $media */
    public $media;

    /** @var People */
    public $people;

    /** @var HttpProxy */
    private $proxy;

    public function __construct($debug = false, $storageConfig = null)
    {
        Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;

        $this->proxy = new HttpProxy();

        $api_args = [$debug, false];
        if(!is_null($storageConfig)){
            array_push($api_args, $storageConfig);
        }

        $this->api = new InstagramProxy(new Instagram(...$api_args), $this->proxy);

        $this->account = new Account($this->api);
        $this->hashtag = new Hashtag($this->api);
        $this->media = new Media($this->api);
        $this->people = new People($this->api);
        $this->timeline = new Timeline($this->api);

    }


    public function setProxy($ip, $port, $ssl = false){
        $this->proxy->setProxy($ip, $port, $ssl);
        $this->api->setProxy($this->proxy->getProxy());
    }

    public function setVerifySSL(bool $ssl){
        $this->api->setVerifySSL($ssl);
    }

    public function setDebug(bool $debug){
        $this->api->debug = $debug;
    }

    public function getProxy(){
        return $this->proxy;
    }

}