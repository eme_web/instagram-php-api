<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 3/19/19
 * Time: 4:30 PM
 */

namespace App\Instagram;


use App\Instagram\Exceptions\AccountDeletionRequestException;
use App\Instagram\Exceptions\DisabledAccountException;
use App\Instagram\Exceptions\InvalidRankTokenException;
use App\Instagram\Exceptions\LinkNotAllowedException;
use App\Instagram\Exceptions\UserRestrictedException;
use InstagramAPI\Exception\AccountDisabledException;
use InstagramAPI\Exception\BadRequestException;
use InstagramAPI\Exception\InstagramException;
use App\Instagram\Exceptions\ActionNotAllowedException;
use App\Instagram\Exceptions\BlockException;
use App\Instagram\Exceptions\FeatureNotAvailableException;
use App\Instagram\Exceptions\MaxAccountsFollowedException;
use App\Instagram\Exceptions\MediaNotFoundException;
use InstagramAPI\Exception\EndpointException;
use InstagramAPI\Exception\FeedbackRequiredException;
use InstagramAPI\Exception\NetworkException;
use InstagramAPI\Exception\NotFoundException;
use InstagramAPI\Exception\SettingsException;
use InstagramAPI\Exception\ThrottledException;
use InstagramAPI\Exception\EmptyResponseException;
use App\Instagram\Exceptions\ProxyException;
use App\Services\Log\Log;

trait ThrowsDerivedInstagramExceptions
{
    public function throwIfInvalidTankTokenException(\InvalidArgumentException $argumentException){
        $message = $argumentException->getMessage();
        if($argumentException instanceof \InvalidArgumentException){
            if($this->_matchString("is not a valid rank token", $message)){
                throw new InvalidRankTokenException($message);
            }
            throw $argumentException;
        }
    }

    public function throwDerivedException(InstagramException $instagramException, HttpProxy $proxy = null){
        $message = $instagramException->getMessage();

        if($this->_matchString("Your account has been disabled for violating our terms", $message))
        {
            throw new AccountDisabledException($message);
        }

        if($this->_matchString("Looks like you requested to delete this account", $message) || $this->_matchString("finished disabling your account", $message))
        {
            throw new AccountDeletionRequestException($message);
        }

        else if($this->_matchString("Please wait a few minutes before you try again", $message))
        {
            throw new ThrottledException($message);
        }

        else if($this->_matchString("following the max limit of accounts", $message))
        {
            throw new MaxAccountsFollowedException($message);
        }
        else if($this->_matchString("Media not found or unavailable", $message))
        {
            throw new MediaNotFoundException($message);
        }
        else if($this->_matchString("User restricted", $message)){
            throw new UserRestrictedException($message);
        }


        if($instagramException instanceof NetworkException){
            if(!is_null($proxy)){
                $ip = $proxy->getIp();
                if($this->_matchString("Failed to connect to ".$ip, $message)){
                    throw new ProxyException($message);
                }
            }
        }

        if($instagramException instanceof EmptyResponseException){
            throw $instagramException;
        }

        if($instagramException instanceof NotFoundException){
            throw $instagramException;
        }

        if($instagramException instanceof BadRequestException){
            throw $instagramException;
        }

        if($instagramException instanceof FeedbackRequiredException || $instagramException instanceof EndpointException){


            try{
                $response = $instagramException->getResponse()->asArray();
            }
            catch (\Exception $exception){
                Log::error($exception->getMessage());
                throw new EndpointException($exception->getMessage());
            }

            $feedback_title = $response["feedback_title"] ?? null;
            $feedback_message = $response["feedback_message"] ?? null;
            if($this->_matchString("Temporarily Blocked", $feedback_title) || $this->_matchString("Action Blocked", $feedback_title))
            {
                throw new BlockException($feedback_message);
            }
            else if($this->_matchString("Link not allowed", $feedback_title)){
                throw new LinkNotAllowedException($feedback_message);
            }

            else if($this->_matchString("Action Not Allowed", $feedback_title))
            {
                throw new ActionNotAllowedException($feedback_message);
            }
            else if($this->_matchString("this feature isn't available right now", $feedback_title))
            {
                throw new FeatureNotAvailableException($feedback_message);
            }

        }
        throw $instagramException;
    }

    private function _matchString($pattern, $subject){
        return preg_match("/".preg_quote($pattern, "/")."/", $subject) === 1;
    }

}