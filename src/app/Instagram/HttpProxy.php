<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/21/19
 * Time: 12:07 PM
 */

namespace App\Instagram;


class HttpProxy
{
    /** @var bool */
    private $ssl;
    /** @var string */
    private $ip;
    /** @var int */
    private $port;

    public function getProxy(){
        return $this->_getProtocol()."://".$this->ip.":".$this->port;
    }

    public function setProxy($ip, $port, $ssl = false){
        $this->ssl = $ssl;
        $this->ip = $ip;
        $this->port = $port;
    }

    public function getIp(){
        return $this->ip;
    }

    private function _getProtocol(){
        return $this->ssl ? "https" : "http";
    }

}