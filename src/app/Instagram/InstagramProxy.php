<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/7/19
 * Time: 5:46 PM
 */

namespace App\Instagram;




use InstagramAPI\Exception\InstagramException;


class InstagramProxy
{
    use ThrowsDerivedInstagramExceptions;

    /** @var InstagramAppApi */
    public $proxyInstagram;

    /** @var HttpProxy */
    public $proxy;


    public function __construct($proxyInstagram, HttpProxy $proxy)
    {
        $this->proxyInstagram = $proxyInstagram;
        $this->proxy = $proxy;
    }

    public function __get($name)
    {
        return $this->proxyInstagram->$name;
    }


    public function __call($methodName, $arguments)
    {
        if (is_callable([$this->proxyInstagram, $methodName]))
        {
            try{
                return call_user_func(array($this->proxyInstagram, $methodName), ...$arguments);
            }
            catch (\InvalidArgumentException $argumentException){
                $this->throwIfInvalidTankTokenException($argumentException);
            }
            catch (InstagramException $instagramException){
                $this->throwDerivedException($instagramException, $this->proxy);
            }
        }
        else
        {
            $class = get_class($this->proxyInstagram);
            throw new \BadMethodCallException("No callable method $methodName at $class class");
        }
    }



}