<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/6/19
 * Time: 11:37 AM
 */

namespace App\Instagram\Requests;

use App\Instagram\ThrowsDerivedInstagramExceptions;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Exception\SettingsException;
use InstagramAPI\Instagram;

abstract class Request
{
    use ThrowsDerivedInstagramExceptions;

    /** @var Instagram */
    protected $api;

    protected $_requestClassName;
    protected $_mapMethods;

    public function __construct($api)
    {
        $this->api = $api;
    }

    protected function _generateRankToken(){
        return \InstagramAPI\Signatures::generateUUID();
    }

    public function __call($methodName, $arguments)
    {
        $className = $this->_requestClassName;
        $methodName = $this->_mapMethods[$methodName];

        if (is_callable([$this->api->proxyInstagram->$className, $methodName]))
        {
            try{
                return call_user_func(array($this->api->proxyInstagram->$className, $methodName), ...$arguments);
            }
            catch (\InvalidArgumentException $argumentException){
                $this->throwIfInvalidTankTokenException($argumentException);
            }
            catch (InstagramException $instagramException){
                $this->throwDerivedException($instagramException, $this->api->proxy);
            }
        }
        else
        {
            throw new \BadMethodCallException("No callable method $methodName at $className class");
        }
    }





}