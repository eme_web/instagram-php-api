<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/8/19
 * Time: 9:20 AM
 */

namespace App\Instagram\Requests;


use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Instagram;
use InstagramAPI\Response\MediaInfoResponse;

/**
 * Class Media
 * @package App\Instagram\Requests
 * @method \InstagramAPI\Response\MediaLikersResponse getMediaLikers($mediaId)
 * @method \InstagramAPI\Response\GenericResponse like($mediaId)
 * @method mixed unlike($mediaId)
 */
class Media extends Request
{
    protected $_requestClassName = "media";
    protected $_mapMethods = [
        "getMediaLikers" => "getLikers",
        "like" => "like",
        "unlike" => "unlike"
    ];

    /**
     * @param $mediaId
     * @return \InstagramAPI\Response
     */
    public function getMediaInfo($mediaId){
        try{
            return $this->api->request("media/{$mediaId}/info/")
                ->getResponse(new MediaInfoResponse);
        }
        catch (\InvalidArgumentException $argumentException){
            $this->throwIfInvalidTankTokenException($argumentException);
        }
        catch (InstagramException $instagramException){
            $this->throwDerivedException($instagramException, $this->api->proxy);
        }
    }

}