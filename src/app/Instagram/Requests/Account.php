<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/6/19
 * Time: 5:08 PM
 */

namespace App\Instagram\Requests;

use App\Instagram\Exceptions\TwoFactorRequiredException;
use GuzzleHttp\Psr7\Response as HttpResponse;

use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Response;

/**
 * Class Account
 * @package App\Instagram\Requests
 * @method \InstagramAPI\Response\UserInfoResponse getCurrentUser()
 */
class Account extends Request
{
    protected $_requestClassName = "account";
    protected $_mapMethods = [
        "getCurrentUser" => "getCurrentUser"
    ];

    /**
     * @param $username
     * @param $password
     * @return Response
     */
    public function login($username, $password)
    {
        $full_login = true;
        try{
            $response = $this->api->login($username, $password);
        }
        catch (\InvalidArgumentException $argumentException){
            $this->throwIfInvalidTankTokenException($argumentException);
        }
        catch (InstagramException $instagramException){
            $this->throwDerivedException($instagramException, $this->api->proxy);
        }

        if(!is_null($response) && $response->getTwoFactorRequired()){
            throw new TwoFactorRequiredException($response);
        }

        if(is_null($response)){
            $full_login = false;
        }
        $instagramResponse = new Response(["status" => "ok", "full_login" => $full_login]);
        $instagramResponse->setHttpResponse(new HttpResponse(200));
        return $instagramResponse;
    }


    public function getSelfUsernameInfo(){
        try{
            return $this->api->people->getSelfInfo();
        }
        catch (\InvalidArgumentException $argumentException){
            $this->throwIfInvalidTankTokenException($argumentException);
        }
        catch (InstagramException $instagramException){
            $this->throwDerivedException($instagramException, $this->api->proxy);
        }

    }

}