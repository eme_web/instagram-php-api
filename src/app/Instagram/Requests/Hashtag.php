<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/8/19
 * Time: 9:16 AM
 */

namespace App\Instagram\Requests;
use InstagramAPI\Exception\InstagramException;

/**
 * Class Hashtag
 * @package App\Instagram\Requests
 * @method \InstagramAPI\Response\SearchTagResponse searchHashtag($hashtag)
 */
class Hashtag extends Request
{
    protected $_requestClassName = "hashtag";
    protected $_mapMethods = [
        "searchHashtag" => "search"
    ];

    /**
     * @param $hashtag
     * @return \InstagramAPI\Response\TagFeedResponse
     */
    public function getTagFeed($hashtag){
        try{
            $rank_token = $this->_generateRankToken();
            return $this->api->hashtag->getFeed($hashtag, $rank_token);
        }
        catch (\InvalidArgumentException $argumentException){
            $this->throwIfInvalidTankTokenException($argumentException);
        }
        catch (InstagramException $instagramException){
            $this->throwDerivedException($instagramException, $this->api->proxy);
        }

    }

}