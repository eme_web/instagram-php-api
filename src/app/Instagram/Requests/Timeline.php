<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/13/19
 * Time: 9:43 AM
 */

namespace App\Instagram\Requests;

/**
 * Class Timeline
 * @package App\Instagram\Requests
 * @method \InstagramAPI\Response\UserFeedResponse getSelfUserFeed()
 */
class Timeline extends Request
{
    protected $_requestClassName = "timeline";
    protected $_mapMethods = [
        "getSelfUserFeed" => "getSelfUserFeed"
    ];

}