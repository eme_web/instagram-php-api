<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/8/19
 * Time: 9:16 AM
 */

namespace App\Instagram\Requests;


use App\Instagram\Exceptions\RestrictedProfileException;
use App\Instagram\ThrowsDerivedInstagramExceptions;
use InstagramAPI\Exception\InstagramException;

/**
 * Class People
 * @package App\Instagram\Requests
 * @method \InstagramAPI\Response\UserInfoResponse getUsernameInfo($username)
 * @method \InstagramAPI\Response\UserInfoResponse getUserInfoById($userId)
 * @method \InstagramAPI\Response\FriendshipResponse follow($userId)
 * @method \InstagramAPI\Response\FriendshipResponse unfollow($userId)
 * @method \InstagramAPI\Response\SearchUserResponse searchUser($userId)
 */
class People extends Request
{
    use ThrowsDerivedInstagramExceptions;

    protected $_requestClassName = "people";
    protected $_mapMethods = [
        "getUsernameInfo" => "getInfoByName",
        "getUserInfoById" => "getInfoById",
        "follow" => "follow",
        "unfollow" => "unfollow",
        "getFriendshipStatus" => "getFriendship",
        "searchUser" => "search"
    ];

    /**
     * @param $userId
     * @param null $maxId
     * @return \InstagramAPI\Response\FollowerAndFollowingResponse
     */
    public function getFollowers($userId, $maxId = null){
        return $this->_getFollowersOrFollowings("getFollowers", $userId, $maxId);
    }

    /**
     * @param $userId
     * @return \InstagramAPI\Response\FollowerAndFollowingResponse
     */
    public function getFollowing($userId, $maxId = null){
        return $this->_getFollowersOrFollowings("getFollowing", $userId, $maxId);
    }

    /**
     * @param $userId
     * @param null $maxId
     * @return \InstagramAPI\Response\FollowerAndFollowingResponse
     */
    public function getSelfFollowers($maxId = null){
        return $this->_getFollowersOrFollowings("getSelfFollowers", $maxId);
    }

    /**
     * @param $userId
     * @param null $maxId
     * @return \InstagramAPI\Response\FollowerAndFollowingResponse
     */
    public function getSelfFollowing($maxId = null){
        return $this->_getFollowersOrFollowings("getSelfFollowing", $maxId);
    }

    /**
     * @param string $method
     * @param $userId
     * @param $maxId
     * @return \InstagramAPI\Response\FollowerAndFollowingResponse
     */
    protected function _getFollowersOrFollowings($method, $userId = null, $maxId = null){
        try{
            $rank_token = $this->_generateRankToken();
            $args = [$userId, $rank_token, null, $maxId];
            if(is_null($userId)){
                $args = [$rank_token, null, $maxId];
            }
            return $this->api->people->$method(...$args);
        }
        catch (\InvalidArgumentException $argumentException){
            $this->throwIfInvalidTankTokenException($argumentException);
        }
        catch (InstagramException $instagramException){
            $this->throwDerivedException($instagramException);
        }

    }

    /**
     * @param $userId
     * @return \InstagramAPI\Response\FriendshipsShowResponse
     */
    public function getFriendshipStatus($userId){
        try{
            $response = $this->api->people->getFriendship($userId);
        }
        catch (\InvalidArgumentException $argumentException){
            $this->throwIfInvalidTankTokenException($argumentException);
        }
        catch (InstagramException $instagramException){
            $this->throwDerivedException($instagramException);
        }

        if(is_null($response->getFollowing())){
            throw new RestrictedProfileException();
        }
        return $response;
    }


}