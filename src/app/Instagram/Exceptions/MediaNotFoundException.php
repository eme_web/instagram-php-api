<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/6/19
 * Time: 11:01 AM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class MediaNotFoundException extends InstagramException
{

}