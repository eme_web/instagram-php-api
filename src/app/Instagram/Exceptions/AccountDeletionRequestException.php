<?php
/**
 * Created by PhpStorm.
 * User: Camilo
 * Date: 23/3/2019
 * Time: 20:31
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class AccountDeletionRequestException extends InstagramException
{

}