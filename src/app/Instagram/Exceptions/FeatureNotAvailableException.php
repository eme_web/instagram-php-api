<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/6/19
 * Time: 11:00 AM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class FeatureNotAvailableException extends InstagramException
{

}