<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 4/23/19
 * Time: 2:40 PM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class LinkNotAllowedException extends InstagramException
{

}