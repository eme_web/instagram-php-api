<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 4/24/19
 * Time: 5:38 PM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class RestrictedProfileException extends InstagramException
{

}