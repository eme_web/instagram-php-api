<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 3/22/19
 * Time: 3:54 PM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class UserRestrictedException extends InstagramException
{

}