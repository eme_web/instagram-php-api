<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/6/19
 * Time: 10:59 AM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class ActionNotAllowedException extends InstagramException
{

}