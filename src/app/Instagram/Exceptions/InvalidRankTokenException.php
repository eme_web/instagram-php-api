<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 3/20/19
 * Time: 7:17 PM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;

class InvalidRankTokenException extends InstagramException
{

}