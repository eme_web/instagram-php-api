<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 3/7/19
 * Time: 7:48 PM
 */

namespace App\Instagram\Exceptions;


use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Response\LoginResponse;
use InstagramAPI\Response\Model\PhoneVerificationSettings;
use InstagramAPI\Response\Model\TwoFactorInfo;
use Throwable;

class TwoFactorRequiredException extends InstagramException
{
    /** @var PhoneVerificationSettings */
    public $phoneVerificationSettings;

    /** @var TwoFactorInfo */
    public $twoFactorInfo;

    public function __construct(LoginResponse $response, int $code = 0, Throwable $previous = null)
    {
        parent::__construct("", $code, $previous);
        $this->phoneVerificationSettings = $response->getPhoneVerificationSettings();
        $this->twoFactorInfo = $response->getTwoFactorInfo();
    }

}