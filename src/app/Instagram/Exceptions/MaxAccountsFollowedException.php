<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/6/19
 * Time: 10:03 AM
 */

namespace App\Instagram\Exceptions;

use InstagramAPI\Exception\InstagramException;

class MaxAccountsFollowedException extends InstagramException
{

}