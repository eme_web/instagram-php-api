<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 3/13/19
 * Time: 2:16 PM
 */

namespace App\Services\Log;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;


class Log
{
    /** @var Logger */
    protected static $log;

    private function __construct(){ }

    private static function create(){
        self::$log = new Logger('name');
        $dir = __DIR__."/../../../logs/instagram.log";
        self::$log->pushHandler(new StreamHandler($dir));
    }

    public static function singleton(){
        if(is_null(self::$log)){
            self::create();
        }
    }

    public static function info($message){
        self::singleton();
        self::$log->info($message);
    }

    public static function error($message){
        self::singleton();
        self::$log->error($message);
    }

}