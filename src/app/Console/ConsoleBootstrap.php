<?php
/**
 * Created by PhpStorm.
 * UserClass: eme
 * Date: 1/22/19
 * Time: 2:46 PM
 */

namespace App\Console;


use App\Console\ConsoleOptions;
use GetOpt\ArgumentException\Unexpected;
use GetOpt\GetOpt;
use App\Instagram\InstagramAppApi;
use InstagramAPI\Exception\InstagramException;

class ConsoleBootstrap
{
    /** @var InstagramAppApi */
    public $api;

    /** @var GetOpt */
    public $consoleOptions;

    public function __construct(InstagramAppApi $api)
    {
        $this->consoleOptions = new ConsoleOptions;
        $this->api = $api;
    }

    /**
     * @param null $options
     * @return OutputsToConsole
     * @throws MethodNotFoundException
     */
    public function process($options = null){

        $apiParams = $this->_processOptions($options);

        $class = $apiParams->class;
        $method = $apiParams->method;
        $args = $apiParams->args;

        try{
            $loginResponse = $this->initApi($apiParams->username, $apiParams->password, $apiParams->proxy);
        }
        catch (InstagramException $instagramException){
            return new ApiException($instagramException);
        }

        if($method === "login"){
            return new ApiResponse($loginResponse);
        }

        try{
            $response = $this->api->$class->$method(...$args);
            return new ApiResponse($response);
        }
        catch (InstagramException $instagramException){
            return new ApiException($instagramException);
        }
        catch (\BadMethodCallException $badMethodCallException){
            throw new MethodNotFoundException($method);
        }
    }

    private function _processOptions($options){
        if(is_null($options)){
            return $this->consoleOptions->process();
        }
        else{
            return $this->consoleOptions->process($options);
        }
    }

    public function initApi($username, $password, $proxy){

        if(!is_null($proxy)){
            $parsed_proxy = $this->_parseProxy($proxy);
            $this->api->setProxy($parsed_proxy["ip"], $parsed_proxy["port"], $parsed_proxy["ssl"]);
        }
        return $this->api->account->login($username, $password);
    }

    private function _parseProxy($proxy){
        preg_match("/([a-z]*):\/\/(.*):([0-9]*)/", $proxy, $matches);
        $ssl = $matches[1] === "https";
        return ["ssl" => $ssl, "ip" => $matches[2], "port" => $matches[3]];
    }

}