<?php
/**
 * Created by PhpStorm.
 * UserClass: eme
 * Date: 1/23/19
 * Time: 9:20 AM
 */

namespace App\Console;

use InstagramAPI\Response;

class ApiResponse implements OutputsToConsole
{
    protected $response;
    protected $status;
    protected $data;

    function __construct(Response $response)
    {
        $this->response = $response;
    }

    private function toArray(){
        return [
            'status' => $this->response->getHttpResponse()->getStatusCode(),
            'data'=> $this->response->asArray()
        ];
    }

    public function toString(){
        return json_encode($this->toArray()).PHP_EOL;
    }

    public function printJson(){
        $this->response->printJson();
    }

}