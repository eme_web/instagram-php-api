<?php

namespace App\Console;

use App\Console\Exceptions\UnknownCommandException;
use App\Console\Options\HashtagOptions;
use App\Console\Options\MediaOptions;
use App\Console\Options\PeopleOptions;
use GetOpt\ArgumentException\Unexpected;
use GetOpt\Command;
use GetOpt\GetOpt;
use App\Console\Options\BasicOptions;

class ConsoleOptions
{

    public $username = null;
    public $password = null;
    public $proxy = null;
    public $method = null;

    public $hashtag = null;
    public $mediaId = null;
    public $accountName = null;

    public $consoleException;

    public function __construct()
    {

        $this->getOpt = new GetOpt();
        $this->getOpt->addCommands([
            Command::create("login", null)
                ->addOptions((new BasicOptions)->getOptions()),

            Command::create("getCurrentUser", null)
                ->addOptions((new BasicOptions)->getOptions()),

            Command::create("getSelfUsernameInfo", null)
            ->addOptions((new BasicOptions)->getOptions()),

            Command::create("getUserInfoById", null)
                ->addOptions((new PeopleOptions())->getOptions()),

            Command::create("getUsernameInfo", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("getTagFeed", null)
                ->addOptions((new HashtagOptions)->getOptions()),

            Command::create("getMediaInfo", null)
                ->addOptions((new MediaOptions)->getOptions()),

            Command::create("like", null)
                ->addOptions((new MediaOptions)->getOptions()),

            Command::create("getMediaLikers", null)
                ->addOptions((new MediaOptions)->getOptions()),

            Command::create("follow", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("unfollow", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("getFollowers", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("getFollowing", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("getSelfFollowing", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("getSelfFollowers", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("getFriendshipStatus", null)
                ->addOptions((new PeopleOptions)->getOptions()),

            Command::create("getSelfUserFeed", null)
                ->addOptions((new PeopleOptions)->getOptions()),

        ]);
    }

    /**
     * @param null $options
     * @return ApiParams
     */
    public function process($options = null){

        try{
            if(is_null($options)){
                $this->getOpt->process();
            }
            else{
                $this->getOpt->process($options);
            }
            return new ApiParams($this->getOpt);
        }
        catch (Unexpected $unexpected){
            throw new UnknownCommandException($this->getOpt->getOperands()[0]);

        }

    }
}