<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/5/19
 * Time: 5:30 PM
 */

namespace App\Console;


interface OutputsToConsole
{
    public function toString();

}