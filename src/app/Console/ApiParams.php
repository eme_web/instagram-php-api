<?php

namespace App\Console;

use GetOpt\GetOpt;

class ApiParams
{
    /** @var GetOpt */
    protected $options;

    /** @var array */
    protected $basicParams;

    public $username;
    public $password;
    public $proxy;

    /** @var string */
    public $class;

    /** @var string */
    public $method;

    /** @var array */
    public $args;

    public function __construct(GetOpt $options)
    {
        $this->setBasicParams($options);
        $this->setMethod($options);
        $this->setArgumentsAndClass($options);
    }



    public function setBasicParams(GetOpt $opt){
        $this->username = $opt->getOption("username");
        $this->password = $opt->getOption("password");
        $this->proxy = $opt->getOption("proxy");
    }

    public function setMethod(GetOpt $opt){
        $this->method = $opt->getCommand()->getName();
    }

    public function setArgumentsAndClass(GetOpt $opt){
        $args = [];
        switch ($this->method){
            case "login":
            case "getCurrentUser":
            case "getSelfUsernameInfo":
                $this->class = "account";
                break;
            case "getTagFeed":
                $this->class = "hashtag";
                $args = [$opt->getOption("hashtag")];
                break;
            case "getUsernameInfo":
                $this->class = "people";
                $args = [$opt->getOption("accountname")];
                break;
            case "getUserInfoById":
                $this->class = "people";
                $args = [$opt->getOption("userid")];
                break;
            case "like":
            case "getMediaInfo":
            case "getMediaLikers":
                $this->class = "media";
                $args = [$opt->getOption("mediaid")];
                break;
            case "getFollowers":
            case "getFollowing":
                $this->class = "people";
                $args = [$opt->getOption("userid"), $opt->getOption("maxid")];
                break;
            case "getSelfFollowing":
            case "getSelfFollowers":
                $this->class = "people";
                $args = [$opt->getOption("maxid")];
                break;
            case "follow":
            case "unfollow":
            case "getFriendshipStatus":
                $this->class = "people";
                $args = [$opt->getOption("userid")];
                break;
            case "getSelfUserFeed":
                $this->class = "timeline";
                $args = [$opt->getOption("maxid")];
                break;

            default:
                break;
        }
        $this->args = $args;
    }

}