<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/27/19
 * Time: 3:19 PM
 */

namespace App\Console\Exceptions;

/**
 * Todas las excepciones de la consola heredan de esta clase
 *
 * Class ConsoleException
 * @package App\Console\Exceptions
 */
class ConsoleException extends \Exception
{

}