<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/27/19
 * Time: 3:20 PM
 */

namespace App\Console\Exceptions;


use Throwable;

class UnknownCommandException extends ConsoleException
{
    public function __construct(string $command = "", int $code = 0, Throwable $previous = null)
    {
        $message = "The command $command is unknown";
        parent::__construct($message, $code, $previous);
    }

}