<?php
/**
 * Created by PhpStorm.
 * UserClass: eme
 * Date: 1/24/19
 * Time: 10:59 AM
 */

namespace App\Console;

use Throwable;

class MethodNotFoundException extends \Exception
{
    public function __construct(string $method, int $code = 0, Throwable $previous = null)
    {
        $message = "Could not find method '$method'\n";
        parent::__construct($message, $code, $previous);
    }

}