<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 2/5/19
 * Time: 5:28 PM
 */

namespace App\Console;


use App\Instagram\Exceptions\AccountDeletionRequestException;
use App\Instagram\Exceptions\ActionNotAllowedException;
use App\Instagram\Exceptions\BlockException;
use App\Instagram\Exceptions\DisabledAccountException;
use App\Instagram\Exceptions\FeatureNotAvailableException;
use App\Instagram\Exceptions\InvalidRankTokenException;
use App\Instagram\Exceptions\LinkNotAllowedException;
use App\Instagram\Exceptions\MaxAccountsFollowedException;
use App\Instagram\Exceptions\MediaNotFoundException;
use App\Instagram\Exceptions\ProxyException;
use App\Instagram\Exceptions\RestrictedProfileException;
use App\Instagram\Exceptions\TwoFactorRequiredException;
use App\Instagram\Exceptions\UserRestrictedException;
use InstagramAPI\Exception\AccountDisabledException;
use InstagramAPI\Exception\ChallengeRequiredException;
use InstagramAPI\Exception\CheckpointRequiredException;
use InstagramAPI\Exception\EmptyResponseException;
use InstagramAPI\Exception\EndpointException;
use InstagramAPI\Exception\FeedbackRequiredException;
use InstagramAPI\Exception\ForcedPasswordResetException;
use InstagramAPI\Exception\IncorrectPasswordException;
use InstagramAPI\Exception\InstagramException;
use InstagramAPI\Exception\InvalidUserException;
use InstagramAPI\Exception\LoginRequiredException;
use InstagramAPI\Exception\NetworkException;
use InstagramAPI\Exception\NotFoundException;
use InstagramAPI\Exception\SentryBlockException;
use InstagramAPI\Exception\SettingsException;
use InstagramAPI\Exception\ThrottledException;

class ApiException implements OutputsToConsole
{
    protected $response;

    public function __construct(InstagramException $instagramException)
    {
        $this->_handleException($instagramException);
    }


    protected function _handleException(InstagramException $instagramException){
        if($instagramException instanceof AccountDisabledException)
        {
            $this->_initializeArrayResponse(400, "disabled_account");
        }
        else if($instagramException instanceof InvalidUserException)
        {
            $this->_initializeArrayResponse(404, "invalid_user");
        }
        else if($instagramException instanceof SettingsException){
            $this->_initializeArrayResponse(404, "settings_exception");
        }
        else if($instagramException instanceof InvalidRankTokenException){
            $this->_initializeArrayResponse(400, "invalid_rank_token");
        }
        else if($instagramException instanceof UserRestrictedException){
            $this->_initializeArrayResponse(400, "user_restricted");
        }
        else if($instagramException instanceof AccountDeletionRequestException){
            $this->_initializeArrayResponse(400, "account_deletion_requested");
        }
        else if ($instagramException instanceof SentryBlockException){
            $this->_initializeArrayResponse(400, "sentry_block");
        }
        else if($instagramException instanceof LoginRequiredException){
            $this->_initializeArrayResponse(400, "login_required");
        }
        else if($instagramException instanceof IncorrectPasswordException)
        {
            $this->_initializeArrayResponse(400, "incorrect_password");
        }
        else if($instagramException instanceof  ForcedPasswordResetException){
            $this->_initializeArrayResponse(400, "reset_password");
        }
        else if($instagramException instanceof CheckpointRequiredException)
        {
            $this->_initializeArrayResponse(400, "checkpoint_required");
        }
        else if($instagramException instanceof ChallengeRequiredException)
        {
            $this->_initializeArrayResponse(400, "challenge_required");
        }
        else if($instagramException instanceof NetworkException)
        {
            $this->_initializeArrayResponse(400, "network_exception");
        }
        else if($instagramException instanceof EmptyResponseException)
        {
            $this->_initializeArrayResponse(400, "empty_response");
        }
        else if($instagramException instanceof BlockException){
            $this->_initializeArrayResponse(400, "blocked");
        }

        else if($instagramException instanceof MaxAccountsFollowedException){
            $this->_initializeArrayResponse(400, "max_accounts_followed");
        }

        else if($instagramException instanceof ActionNotAllowedException){
            $this->_initializeArrayResponse(400, "action_not_allowed");
        }

        else if($instagramException instanceof FeatureNotAvailableException){
            $this->_initializeArrayResponse(400, "feature_not_available");
        }

        else if($instagramException instanceof MediaNotFoundException){
            $this->_initializeArrayResponse(404, "media_not_found");
        }
        else if($instagramException instanceof LinkNotAllowedException){
            $this->_initializeArrayResponse(400, "link_not_allowed");
        }

        else if($instagramException instanceof ThrottledException){
            $this->_initializeArrayResponse(429, "throttled");
        }

        else if($instagramException instanceof ProxyException){
            $this->_initializeArrayResponse(400, "proxy_error");
        }

        else if($instagramException instanceof NotFoundException){
            $this->_initializeArrayResponse(400, "not_found_exception");
        }

        else if($instagramException instanceof RestrictedProfileException){
            $this->_initializeArrayResponse(400, "restricted_profile");
        }

        else if($instagramException instanceof EndpointException){
            $this->_initializeArrayResponse(400, "endpoint_exception");
        }
        else if($instagramException instanceof FeedbackRequiredException){
            $this->_initializeArrayResponse(400, "unknown_feedback_required");
            $this->response["full_response"] = $instagramException->getResponse()->asJson();
        }
        else if($instagramException instanceof TwoFactorRequiredException){
            $this->_initializeArrayResponse(400, "two_factor_required");
        }
        else{
            $this->_initializeArrayResponse(400, "instagram_error");
            $this->response["full_response"] = $instagramException->getResponse()->asJson();
        }
        $this->response["message"] = $instagramException->getMessage();
    }

    private function _initializeArrayResponse($status, $code){
        $this->response = ["status" => $status, "code" => $code];
    }

    public function toString()
    {
        return json_encode($this->response).PHP_EOL;
    }

}