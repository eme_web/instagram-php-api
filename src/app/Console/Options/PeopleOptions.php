<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 1/24/19
 * Time: 5:50 PM
 */

namespace App\Console\Options;


use GetOpt\GetOpt;

class PeopleOptions extends BasicOptions
{
    /**
     * TODO: Todos los argumentos son opcionales?? Tratar de optimizar esto separando en una
     * nueva clase de opciones
     */

    protected $extraDefinitions = [
        ["short" => null, "long" => "userid", "mode" => GetOpt::OPTIONAL_ARGUMENT],
        ["short" => null, "long" => "accountname", "mode" => GetOpt::OPTIONAL_ARGUMENT],
        ["short" => null, "long" => "maxid", "mode" => GetOpt::OPTIONAL_ARGUMENT]
    ];

}