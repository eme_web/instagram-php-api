<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 1/24/19
 * Time: 5:34 PM
 */

namespace App\Console\Options;


use GetOpt\GetOpt;

class BasicOptions
{
    protected $definitions = [
        ["short" => "u", "long" => "username", "mode" => GetOpt::REQUIRED_ARGUMENT],
        ["short" => "p", "long" => "password", "mode" => GetOpt::REQUIRED_ARGUMENT],
        ["short" => null, "long" => "proxy", "mode" => GetOpt::OPTIONAL_ARGUMENT]
    ];

    protected $extraDefinitions = [];

    protected $options = null;

    public function __construct()
    {
        $this->_setOptions();
    }

    protected function _setOptions(){
        $this->definitions = array_merge($this->definitions, $this->extraDefinitions);
        $this->options = array_map(function ($opt){
            return \GetOpt\Option::create($opt['short'], $opt['long'], $opt['mode']);
        }, $this->definitions);
    }

    public function getOptions(){
        return $this->options;
    }

}