<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 1/24/19
 * Time: 5:53 PM
 */

namespace App\Console\Options;


use GetOpt\GetOpt;

class MediaOptions extends BasicOptions
{
    protected $extraDefinitions = [
        ["short" => null, "long" => "mediaid", "mode" => GetOpt::REQUIRED_ARGUMENT]
    ];

}