<?php
/**
 * Created by PhpStorm.
 * User: eme
 * Date: 1/24/19
 * Time: 5:48 PM
 */

namespace App\Console\Options;


use GetOpt\GetOpt;

class HashtagOptions extends BasicOptions
{
    protected $extraDefinitions = [
        ["short" => "h", "long" => "hashtag", "mode" => GetOpt::REQUIRED_ARGUMENT]
    ];

}